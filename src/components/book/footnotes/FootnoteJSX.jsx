import { Show, createSignal } from 'solid-js'
import {
  animateAndRemoveElement,
  animateAndShowElement,
} from '@utils/animation'
import styles from '@styles/footnote.module.css'

export function FootnoteJSX(props) {
  const [showFootnotes, setShowFootnotes] = createSignal(false)
  const [footnoteRef, setFootnoteRef] = createSignal()

  return (
    <span class={styles.footnoteSpan}>
      <button
        class={
          showFootnotes()
            ? styles.activeFootnoteButton
            : styles.inactiveFootnoteButton
        }
        onClick={() => {
          if (showFootnotes() === false) {
            setShowFootnotes(true)
            animateAndShowElement(footnoteRef())
          } else {
            animateAndRemoveElement(footnoteRef())

            footnoteRef()?.style.setProperty('--animate-duration', '0.5s')

            footnoteRef()?.addEventListener(
              'animationend',
              () => {
                setShowFootnotes(false)
              },
              { once: true },
            )
          }
        }}
      >
        {props.match}
      </button>
      <Show when={showFootnotes()}>
        <aside
          ref={(element) => setFootnoteRef(element)}
          class={styles.footnotesAside}
        >
          {props.footnote}
        </aside>
      </Show>
    </span>
  )
}
