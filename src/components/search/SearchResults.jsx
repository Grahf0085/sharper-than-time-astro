import { createEffect, createMemo, createSignal, Show } from 'solid-js'
import { createMarker, makeSearchRegex } from '@solid-primitives/marker'
import X from '../../../node_modules/lucide-solid/dist/source/icons/x.jsx'
import LoaderCircle from '../../../node_modules/lucide-solid/dist/source/icons/loader-circle.jsx'
import { useStore } from '@nanostores/solid'
import { searchResults, searchTerm } from '@utils/store'
import { navigate } from 'astro:transitions/client'
import styles from '@styles/searchResults.module.css'

export function SearchResults(props) {
  const [loading, setLoading] = createSignal(false)
  const [searchResultsRef, setSearchResultsRef] = createSignal()

  const $searchResults = useStore(searchResults)
  const $searchTerm = useStore(searchTerm)

  const highlight = createMarker((text) => (
    <mark class={styles.searchResultsMark}>{text()}</mark>
  ))
  const regex = createMemo(() => makeSearchRegex($searchTerm()))

  const clearSearchResultsOnMobile = () => {
    if (window.matchMedia('(max-width: 768px)').matches) {
      searchResults.set([])
      searchTerm.set('')
    }
  }

  const scrollToElement = (paragraphId) => {
    const paragraphToScollTo = document.getElementById(paragraphId)
    paragraphToScollTo.scrollIntoView({ behavior: 'smooth' })
  }

  const scrollToSelectedResult = (paragraph) => {
    if (
      (paragraph.bookTitle && paragraph.bookTitle !== props.title) ||
      (paragraph.translatorName &&
        paragraph.translatorName !== props.translator)
    ) {
      setLoading(true)
      navigate(`/book/${paragraph.bookTitle}/${paragraph.translatorName}`)

      document.addEventListener(
        'astro:page-load',
        () => {
          requestAnimationFrame(() => {
            scrollToElement(paragraph.paragraphId)
            setLoading(false)
            clearSearchResultsOnMobile()
          })
        },
        { once: true },
      )
    } else {
      scrollToElement(paragraph.paragraphId)
      clearSearchResultsOnMobile()
    }
  }

  createEffect(() => {
    searchResultsRef()?.classList.remove('animate__fadeOutRight')
    searchResultsRef()?.classList.add(
      'animate__animated',
      'animate__fadeInRight',
    )
  })

  return (
    <>
      {$searchResults().length > 0 && (
        <div
          class={styles.searchResultsContainer}
          ref={(element) => setSearchResultsRef(element)}
        >
          <Show
            when={!loading()}
            fallback={
              <LoaderCircle size={40} class={`${styles.spinner} spinner`} />
            }
          >
            <div class={styles.searchResultsHeader}>
              <p>Click A Result</p>
              <button
                onClick={() => {
                  searchResultsRef()?.classList.add('animate__fadeOutRight')

                  searchResultsRef().addEventListener(
                    'animationend',
                    () => {
                      searchResults.set([])
                    },
                    { once: true },
                  )
                }}
                class={styles.closeSearchResultsButton}
              >
                <X size={40} />
              </button>
            </div>
          </Show>

          <ol class={styles.searchResultsList}>
            {$searchResults().map((paragraph, index) => {
              /* slice and join used to cut out partial words at beginning */
              /* and end of paragraphText  */
              /* TODO PROBLEM - if first or last word */
              /* is word that was searched for is it being cut off? */
              const trimmedParagraph = paragraph.paragraphText
                .split(' ')
                .slice(1, -1)
                .join(' ')
              return (
                <li class={styles.searchResultsItem}>
                  <button onClick={() => scrollToSelectedResult(paragraph)}>
                    <span class={styles.searchResultsNumber}>
                      {index + 1} -{' '}
                    </span>
                    {paragraph.bookTitle ? (
                      <>
                        <cite>{paragraph.bookTitle}</cite>(
                        {paragraph.translatorName}
                        ),
                        <div>
                          {paragraph.chapterName}, Paragraph{' '}
                          {paragraph.paragraphNumber}
                        </div>
                      </>
                    ) : (
                      <span>
                        {paragraph.chapterName}, Paragraph{' '}
                        {paragraph.paragraphNumber}
                      </span>
                    )}

                    <p>{highlight(trimmedParagraph, regex())}</p>
                  </button>
                </li>
              )
            })}
          </ol>
        </div>
      )}
    </>
  )
}
