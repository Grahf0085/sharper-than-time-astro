import pool from './pool'
import bcrypt from 'bcryptjs'

export async function findUser({
  where: { username = undefined, id = undefined },
}) {
  if (id !== undefined && !isNaN(id)) {
    const result = await pool.query('SELECT * FROM readers WHERE id = $1', [id])
    return result.rows[0]
  } else if (username !== undefined) {
    const result = await pool.query(
      'SELECT * FROM readers WHERE username = $1',
      [username],
    )
    return result.rows[0]
  }
  return null
}

export async function createUser(username, password) {
  const result = await pool.query(
    `
INSERT INTO readers
VALUES (DEFAULT, $1, $2)
RETURNING *
`,
    [
      username,
      bcrypt
        .hashSync(
          password + import.meta.env.PEPPER,
          Number(import.meta.env.SALT_ROUNDS),
        )
        .toString(),
    ],
  )

  await pool.query(
    `
INSERT INTO bookmarks
VALUES (DEFAULT, $1, NULL)
`,
    [result.rows[0].id],
  )
  return result.rows[0]
}

export async function login(username, password) {
  const user = await findUser({ where: { username } })
  if (!user) return null

  let isCorrectPassword = bcrypt.compareSync(
    password + import.meta.env.PEPPER,
    user.digested_password,
  )
  if (!isCorrectPassword) return null
  return user
}
