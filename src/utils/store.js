import { atom } from 'nanostores'

export const selectedFont = atom('Inter')
export const selectedFontSize = atom({ name: 'normal', size: '1rem' })
export const selectedLineHeight = atom({ name: 'Normal', height: '1.5' })
export const selectedMarginSize = atom({ name: 'Medium', size: '3rem' })
export const searchResults = atom([])
export const searchTerm = atom('')
export const selectedBookmark = atom(undefined)
