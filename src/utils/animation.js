export const animateAndShowElement = (element) => {
  element.classList.remove('hidden', 'animate__fadeOut')
  element.classList.add('animate__animated', 'animate__fadeIn')
}

export const animateAndRemoveElement = (element) => {
  element.classList.remove('animate__fadeIn')
  element.classList.add('animate__fadeOut')
}

export const removeAfterFadeOut = (element, sideContainer) => {
  element.addEventListener('animationend', () => {
    if (
      sideContainer.contains(element) &&
      element.classList.contains('animate__fadeOut')
    ) {
      sideContainer.removeChild(element)
    }
  })
}

export const hideAfterFadeOut = (element) => {
  element.addEventListener('animationend', () => {
    if (element.classList.contains('animate__fadeOut')) {
      element.classList.add('hidden')
    }
  })
}
