import { SignJWT, jwtVerify } from 'jose'
import { nanoid } from 'nanoid'
import { findUser } from '@server/auth'

const TOKEN = import.meta.env.TOKEN
const secret = new TextEncoder().encode(import.meta.env.JWT_SECRET_KEY)

export async function createToken(context, userId) {
  const token = await new SignJWT({ userId: userId })
    .setProtectedHeader({ alg: 'HS256' })
    .setJti(nanoid())
    .setIssuedAt()
    .setExpirationTime('48h')
    .sign(secret)

  context.cookies.set(TOKEN, token, {
    httpOnly: true,
    path: '/',
    maxAge: 60 * 60 * 48, // 48 hours in seconds
    secure: true,
    sameSite: 'lax',
  })

  return token
}

const getUserSession = (cookies) => {
  return cookies.get(TOKEN)?.value
}

export const getUserId = async (cookies) => {
  const session = getUserSession(cookies)
  if (session) {
    const jwtVerifyResult = await jwtVerify(session, secret)
    const { userId } = jwtVerifyResult.payload
    if (!userId || typeof userId !== 'number') return null
    return userId
  }
  return null
}

export const getUser = async (cookies) => {
  const userId = await getUserId(cookies)

  if (typeof userId !== 'number') {
    return null
  }

  try {
    const user = await findUser({ where: { id: userId } })
    return user
  } catch (error) {
    console.error('Error getting user: ', error)
    throw error
  }
}
