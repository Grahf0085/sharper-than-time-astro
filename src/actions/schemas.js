import { z } from 'zod'

export const singleBookSearchSchema = z.object({
  term: z
    .string()
    .trim()
    .min(1, { message: 'Please enter a search term' })
    .min(3, { message: 'Please use at least three characters' })
    .max(30, { message: 'Please use less than 31 characters' }),
  title: z.string(),
  translator: z.string(),
})

export const multipleBookSearchSchema = z.object({
  term: z
    .string()
    .trim()
    .min(1, { message: 'Please enter a search term' })
    .min(3, { message: 'Please use at least three characters' })
    .max(30, { message: 'Please use less than 31 characters' }),
  selectedBooks: z
    .array(z.string())
    .nonempty({ message: 'Please select at least one  book to search' }),
})

export const loginSchema = z.object({
  username: z
    .string()
    .trim()
    .min(1, { message: 'Please enter a username' })
    .min(3, { message: 'Usernames must be at least 3 characters' })
    .max(16, { message: 'Usernames should be less than 17 characters' }),
  password: z
    .string()
    .trim()
    .min(1, { message: 'Please enter a password' })
    .min(5, { message: 'Passwords must be at least 5 characters' })
    .max(64, { message: 'Passwords should be less than 65 characters' }),
})

export const registerSchema = loginSchema
  .extend({
    confirmPassword: z.string().trim(),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: 'Passwords do not match',
    path: ['confirmPassword'],
  })

export const bookmarkSchema = z.object({
  userId: z.number().int().positive(),
  paragraphId: z.number().int().positive(),
})

export const notesSchema = z.object({
  userId: z.number().int().positive(),
  paragraphId: z.number().int().positive(),
  notes: z
    .string()
    .trim()
    .min(1, { message: 'Please enter a note' })
    .min(3, { message: 'Note should be at least three characters' })
    .max(3000, { message: 'Note should be less than 3001 characters' }),
})

export const fetchNotesSchema = z.object({
  userId: z.number().int().positive(),
  paragraphId: z.number().int().positive(),
})
