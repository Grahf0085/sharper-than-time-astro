import { defineAction, ActionError } from 'astro:actions'
import { getSingleBookSearch, getMultipleBookSearch } from '@server/book'
import { createBookmark, createNotes, getNotes } from '@server/user'
import { findUser, createUser, login } from '@server/auth'
import {
  singleBookSearchSchema,
  multipleBookSearchSchema,
  loginSchema,
  bookmarkSchema,
  notesSchema,
  fetchNotesSchema,
} from './schemas'
import { createToken } from '@utils/auth.js'

export const server = {
  singleBookSearch: defineAction({
    accept: 'form',
    input: singleBookSearchSchema,
    handler: async ({ term, title, translator }) => {
      try {
        const searchResults = await getSingleBookSearch(term, title, translator)
        return searchResults
      } catch (error) {
        console.error('Single book search failed: ', error)
        throw new ActionError({
          code: 'INTERNAL_SERVER_ERROR',
          message: error.message || error,
        })
      }
    },
  }),

  multiBookSearch: defineAction({
    accept: 'form',
    input: multipleBookSearchSchema,
    handler: async ({ term, selectedBooks }) => {
      try {
        const searchResults = await getMultipleBookSearch(term, selectedBooks)
        return searchResults
      } catch (error) {
        console.error('Multi book search failed: ', error)
        throw new ActionError({
          code: 'INTERNAL_SERVER_ERROR',
          message: error.message || error,
        })
      }
    },
  }),

  registerUser: defineAction({
    accept: 'form',
    /* using loginSchema because I really only need a username and password - two not passwords */
    input: loginSchema,
    handler: async ({ username, password }, context) => {
      await findUser({ where: { username: username } })
        .then((userAlreadyExists) => {
          if (userAlreadyExists) {
            throw new ActionError({
              code: 'CONFLICT',
              message: `Username ${username} is already taken`,
            })
          }
        })
        .catch((error) => {
          console.error('Finding user during registration failed: ', error)
          throw new ActionError({
            code: 'INTERNAL_SERVER_ERROR',
            message: error.message || error,
          })
        })

      try {
        const newUser = await createUser(username, password)

        if (!newUser) {
          throw new ActionError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to register new user',
          })
        }

        const token = await createToken(context, newUser.id)

        if (!token) {
          throw new ActionError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to create token for new user',
          })
        }

        return newUser.id
      } catch (error) {
        console.error('Error creating new user: ', error)
        throw new Error(error.message || error)
      }
    },
  }),

  loginUser: defineAction({
    accept: 'form',
    input: loginSchema,
    handler: async ({ username, password }, context) => {
      try {
        const user = await login(username, password)

        if (!user) {
          throw new ActionError({
            code: 'UNAUTHORIZED',
            message: 'Username/Paassword combination is incorrect',
          })
        }

        const token = await createToken(context, user.id)

        if (!token) {
          throw new ActionError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to create token for user',
          })
        }

        return user.id
      } catch (error) {
        console.error('Error logging in user: ', error)
        throw new Error(error.message || error)
      }
    },
  }),

  logoutUser: defineAction({
    handler: (_, context) => {
      const TOKEN = import.meta.env.TOKEN

      try {
        context.cookies.set(TOKEN, '', {
          httpOnly: true,
          maxAge: 0,
          path: '/',
          secure: true,
          sameSite: 'lax',
        })

        return { cookieDeleted: true }
      } catch (error) {
        console.error('Error logging out: ', error)
        throw new Error(error.message || error)
      }
    },
  }),

  makeBookmark: defineAction({
    accept: 'json',
    input: bookmarkSchema,
    handler: async ({ userId, paragraphId }) => {
      try {
        const bookmark = await createBookmark(userId, paragraphId)

        if (!bookmark) {
          throw new ActionError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to make bookmark',
          })
        }

        return bookmark
      } catch (error) {
        console.error('Error creating bookmark: ', error)
        throw new Error(error.message || error)
      }
    },
  }),

  makeNotes: defineAction({
    accept: 'form',
    input: notesSchema,
    handler: async ({ userId, paragraphId, notes }) => {
      const affectedRows = await createNotes(userId, paragraphId, notes)

      if (affectedRows !== 1) {
        throw new ActionError({
          code: 'INTERNAL_SERVER_ERROR',
          message: 'Failed to make notes',
        })
      }
    },
  }),

  fetchNotes: defineAction({
    accept: 'json',
    input: fetchNotesSchema,
    handler: async ({ userId, paragraphId }) => {
      const fetchedNotes = await getNotes(userId, paragraphId)

      if (!fetchedNotes) {
        return { notes: '' }
      }

      return { notes: fetchedNotes }
    },
  }),
}
