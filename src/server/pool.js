import pg from 'pg'

const Pool = pg.Pool

const mode = import.meta.env.PGSSLMODE === 'require'

const pool = new Pool({
  connectionString: import.meta.env.DATABASE_URL,
  ssl: mode,
})

export default pool
