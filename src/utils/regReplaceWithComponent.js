/* stolen from https://gist.github.com/edivados/0b24fd7e876d1940fd273361a31ffe1c */

export function regReplaceWithComponent(text, exp, fn) {
  const matches = Array.from(text.matchAll(exp))
  const items = []

  for (let index = 0; index < matches.length; index++) {
    const match = matches[index]
    const prevMatch = index ? matches[index - 1] : undefined
    const offset = prevMatch ? prevMatch.index + prevMatch[0].length : 0

    items.push(text.slice(offset, match.index))
    items.push(fn(match[0], index))

    const isLastMatch = index === matches.length - 1
    const hasTextAfterLastMatch = match.index + match[0].length < text.length

    if (isLastMatch && hasTextAfterLastMatch) {
      items.push(text.slice(match.index + match[0].length))
    }
  }

  return items.length ? items : text
}
