import { defineConfig } from 'astro/config'
import solidJs from '@astrojs/solid-js'
import vercel from '@astrojs/vercel/serverless'

import sitemap from '@astrojs/sitemap'

// https://astro.build/config
export default defineConfig({
  site: 'https://www.sharper-than-time.com',
  integrations: [solidJs(), sitemap()],
  output: 'hybrid',
  experimental: {
    actions: true,
  },
  adapter: vercel({
    maxDuration: 60,
  }),
})
