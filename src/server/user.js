import pool from './pool'

export async function createBookmark(userId, paragraphId) {
  await pool.query(
    `
UPDATE bookmarks
SET paragraph_fk = $2
WHERE reader_fk = $1
RETURNING *
    `,
    [userId, paragraphId],
  )

  const { rows } = await pool.query(
    `
SELECT translator_name AS "translatorName", book_title AS "bookTitle", chapter_name AS "chapterName", paragraph_number AS "paragraphNumber", paragraphs.id AS "paragraphId" FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE paragraphs.id = (SELECT paragraph_fk FROM bookmarks WHERE reader_fk = $1)
`,
    [userId],
  )
  return rows[0]
}

export async function getBookmark(userId) {
  const { rows } = await pool.query(
    `
SELECT translator_name AS "translatorName", book_title AS "bookTitle", chapter_name AS "chapterName", paragraph_number AS "paragraphNumber", paragraphs.id AS "paragraphId" 
FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE paragraphs.id = (SELECT paragraph_fk FROM bookmarks WHERE reader_fk = $1)
`,
    [userId],
  )
  return rows[0]
}

export async function createNotes(userId, paragraphId, notes) {
  const result = await pool.query(
    `
MERGE INTO notes AS target
USING (VALUES ($1::integer, $2::smallint, $3::text)) AS source (reader_fk, paragraph_fk, notes)
ON (target.reader_fk = source.reader_fk AND target.paragraph_fk = source.paragraph_fk)
WHEN matched THEN 
UPDATE SET notes = source.notes, last_edited = DEFAULT
WHEN NOT matched THEN
INSERT (id, reader_fk, paragraph_fk, notes, last_edited)
VALUES (DEFAULT, source.reader_fk, source.paragraph_fk, source.notes, DEFAULT)
`,
    [userId, paragraphId, notes],
  )

  return result.rowCount
}

export async function getNotes(userId, paragraphId) {
  const { rows } = await pool.query(
    `
SELECT notes FROM notes
WHERE reader_fk = $1 AND paragraph_fk = $2
`,
    [userId, paragraphId],
  )
  if (rows.length > 0) return rows[0].notes
  else return ''
}

export async function getAllNotesForReader(userId) {
  const { rows } = await pool.query(
    `
SELECT paragraph_number AS "paragraphNumber", chapter_name AS "chapterName", book_title AS "bookTitle", translator_name AS "translatorName", notes, last_edited AS "lastEdited"
FROM notes
INNER JOIN paragraphs ON paragraphs.id = notes.paragraph_fk 
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE notes.reader_fk = $1
`,
    [userId],
  )
  return rows
}
