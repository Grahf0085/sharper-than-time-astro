import {
  Show,
  createRenderEffect,
  createSignal,
  onCleanup,
  onMount,
} from 'solid-js'
import { navigate } from 'astro:transitions/client'
import { useStore } from '@nanostores/solid'
import Bookmark from '../../../node_modules/lucide-solid/dist/source/icons/bookmark.jsx'
import BookmarkX from '../../../node_modules/lucide-solid/dist/source/icons/bookmark-x.jsx'
import LoaderCircle from '../../../node_modules/lucide-solid/dist/source/icons/loader-circle.jsx'
import { selectedBookmark } from '@utils/store'
import {
  animateAndShowElement,
  animateAndRemoveElement,
} from '@utils/animation.js'
import styles from '@styles/gotoBookmark.module.css'

export function GotoBookmark(props) {
  const [loading, setLoading] = createSignal(false)
  const [headerBookmarkButtonRef, setHeaderBookmarkButtonRef] = createSignal()
  const [bookmarkInfoRef, setBookmarkInfoRef] = createSignal()
  const [errorRef, setErrorRef] = createSignal()
  const [usersBookmark, setUsersBookmark] = createSignal()

  createRenderEffect(() => {
    selectedBookmark.set(props.currentBookmark)

    const $selectedBookmark = useStore(selectedBookmark)
    setUsersBookmark($selectedBookmark())
  })

  onMount(() => {
    /* we need to change the icon of the active bookmark to a checkmark when the page loads  */

    const checkedBookmarkIcon = document.getElementById(
      `activeBookmark${usersBookmark()?.paragraphId}`,
    )

    const uncheckedBookmarkIcon = document.getElementById(
      `inactiveBookmark${usersBookmark()?.paragraphId}`,
    )

    checkedBookmarkIcon?.classList.remove('hidden')
    uncheckedBookmarkIcon?.classList.add('hidden')

    const handleMouseOver = () => {
      bookmarkInfoRef() && animateAndShowElement(bookmarkInfoRef())
      errorRef() && animateAndShowElement(errorRef())
    }

    const handleMouseLeave = () => {
      bookmarkInfoRef() && animateAndRemoveElement(bookmarkInfoRef())
      errorRef() && animateAndRemoveElement(errorRef())
    }

    headerBookmarkButtonRef()?.addEventListener('mouseover', handleMouseOver)

    headerBookmarkButtonRef()?.addEventListener('mouseleave', handleMouseLeave)

    onCleanup(() => {
      headerBookmarkButtonRef()?.removeEventListener(
        'mouseover',
        handleMouseOver,
      )
      headerBookmarkButtonRef()?.removeEventListener(
        'mouseleave',
        handleMouseLeave,
      )
    })
  })

  const scrollToElement = (paragraphId) => {
    const paragraphToScollTo = document.getElementById(paragraphId)
    paragraphToScollTo.scrollIntoView({ behavior: 'smooth' })
  }

  const scrollToSelectedResult = (paragraph) => {
    if (
      (paragraph.bookTitle && paragraph.bookTitle !== props.currentTitle) ||
      (paragraph.translatorName &&
        paragraph.translatorName !== props.currentTranslator)
    ) {
      setLoading(true)
      navigate(`/book/${paragraph.bookTitle}/${paragraph.translatorName}`)

      document.addEventListener(
        'astro:page-load',
        () => {
          requestAnimationFrame(() => {
            scrollToElement(paragraph.paragraphId)
            setLoading(false)
          })
        },
        { once: true },
      )
    } else {
      scrollToElement(paragraph.paragraphId)
    }
  }

  return (
    <div class={styles.bookmarkContainer}>
      <Show
        when={!loading()}
        fallback={
          <div>
            <LoaderCircle class={`${styles.spinner} spinner`} />
          </div>
        }
      >
        <button
          ref={(element) => setHeaderBookmarkButtonRef(element)}
          onClick={() => scrollToSelectedResult(usersBookmark())}
          disabled={props.bookmarkError}
        >
          <Show when={props.bookmarkError}>
            <BookmarkX class={styles.bookmarkSvg} />
          </Show>
          <Show when={props.currentBookmark}>
            <Bookmark class={styles.bookmarkSvg} />
          </Show>
        </button>
      </Show>

      <Show when={usersBookmark()}>
        <div
          ref={(element) => setBookmarkInfoRef(element)}
          class={`${styles.bookmarkInfo} hidden`}
        >
          <p class={styles.bookmarkLine}>
            {usersBookmark().bookTitle}({usersBookmark().translatorName}
            ),
          </p>
          <p class={styles.bookmarkLine}>{usersBookmark().chapterName}, </p>
          <p class={styles.bookmarkLine}>
            Paragraph {usersBookmark().paragraphNumber}
          </p>
        </div>
      </Show>

      <Show when={props.bookmarkError}>
        <p
          ref={(element) => setErrorRef(element)}
          class={`${styles.bookmarkInfo} hidden`}
        >
          {props.bookmarkError}
        </p>
      </Show>
    </div>
  )
}
